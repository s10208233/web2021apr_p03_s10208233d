﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using web_s10208233.Models;
using Microsoft.AspNetCore.Http;

namespace web_s10208233.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult StudentLogin()
        {
            HttpContext.Session.SetString("Role", "Student");
            return RedirectToAction("Index", "Book");
        }

        [HttpPost]
        public ActionResult StaffLogin(IFormCollection formData)
        {
            //Read inputs from textboxes
            // Email address converted to lowercase
            string loginID = formData["txtLoginId"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();
            if (loginID == "abc@npbook.com" && password == "pass1234")
            {
                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);

                // Store user role “Staff” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Staff");

                // Challenge login time
                HttpContext.Session.SetString("LoginTime",DateTime.Now.ToString("dd-MMMM-yyyy hh:mm:ss tt"));


                // Redirect user to the "StaffMain" view through an action
                return RedirectToAction("StaffMain");
            }
            else
            {
                // Store an error message in TempData for display at the index view
                TempData["Message"] = "Invalid Login Credentials!";

                // Redirect user back to the index view through an action 
                return RedirectToAction("Index");
            }
        }

        public ActionResult StaffMain()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            TimeSpan loginDuration = DateTime.Now - Convert.ToDateTime(HttpContext.Session.GetString("LoginTime"));
            TempData["loginDuration"] = "You have logged in for " + loginDuration.Seconds + " seconds.";

            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();

            // Call the Index action of Home controller
            return RedirectToAction("Index");
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new  { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
