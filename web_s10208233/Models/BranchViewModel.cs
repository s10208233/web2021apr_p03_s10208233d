﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_s10208233.Models
{
    public class BranchViewModel
    {
        public List<Branch> branchList { get; set; }
        public List<Staff> staffList { get; set; }
        public BranchViewModel()
        {
            branchList = new List<Branch>();
            staffList = new List<Staff>();
        }
    }
}
