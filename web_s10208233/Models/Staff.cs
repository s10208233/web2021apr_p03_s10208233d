﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_s10208233.Models
{
    public class Staff
    {
        [Display(Name="ID")]
        public int StaffId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Cannot exceed 50 charaters")]
        public string Name { get; set; }
        public char Gender { get; set; }

        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }
        public string Nationality { get; set; }
        [Display(Name = "Email Address")]
        [EmailAddress]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExist]

        public string Email { get; set; }
        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00, 10000.00, ErrorMessage = "Salary must be between 1.00 to 10,000.00")]
        public decimal Salary { get; set; }
        [Display(Name="Full-Time Staff")]
        public bool IsFullTime { get; set; }
        public int? BranchNo { get; set; }


    }
}
